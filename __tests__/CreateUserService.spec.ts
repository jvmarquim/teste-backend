import AppError from '../src/errors/AppError';
import FakeUsersRepository from '../src/repositories/fakes/FakeUsersRepository';

import CreateUserService from '../src/services/CreateUserService';

let fakeUsersRepository: FakeUsersRepository;
let createUser: CreateUserService;

describe('CreateUser', () => {
  beforeEach(() => {
    fakeUsersRepository = new FakeUsersRepository();

    createUser = new CreateUserService(fakeUsersRepository);
  });

  it('should be able to create user', async () => {
    const user = await createUser.execute({
      name: 'John Doe',
      email: 'test@email.com',
      password: 'password',
    });

    expect(user).toHaveProperty('id');
  });

  it('should not be able to create user with email already in use', async () => {
    await createUser.execute({
      name: 'John Doe',
      email: 'test@email.com',
      password: 'password',
    });

    await expect(
      createUser.execute({
        name: 'John Doe',
        email: 'test@email.com',
        password: 'password',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });
});
