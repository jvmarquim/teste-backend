import 'dotenv/config';
import AppError from '../src/errors/AppError';
import FakeUsersRepository from '../src/repositories/fakes/FakeUsersRepository';

import CreateUserService from '../src/services/CreateUserService';
import UpdateUserService from '../src/services/UpdateUserService';
import AuthenticateUserService from '../src/services/AuthenticateUserService';

let fakeUsersRepository: FakeUsersRepository;
let createUser: CreateUserService;
let updateUser: UpdateUserService;
let authenticateUSer: AuthenticateUserService;

describe('UpdateUser', () => {
  beforeEach(() => {
    fakeUsersRepository = new FakeUsersRepository();

    updateUser = new UpdateUserService(fakeUsersRepository);
    createUser = new CreateUserService(fakeUsersRepository);
    authenticateUSer = new AuthenticateUserService(fakeUsersRepository);
  });

  it('should be able to update user', async () => {
    const user = await createUser.execute({
      name: 'John Doe',
      email: 'test@email.com',
      password: 'password',
    });

    const updatedUser = await updateUser.execute({
      user_id: user.id,
      name: 'John Doe 2',
      email: 'test2@email.com',
      old_password: 'password',
      password: 'new-password',
    });

    const authenticated = await authenticateUSer.execute({
      email: 'test2@email.com',
      password: 'new-password',
    });

    expect(updatedUser.name).toEqual('John Doe 2');
    expect(updatedUser.email).toEqual('test2@email.com');
    expect(authenticated).toHaveProperty('token');
  });

  it('should not be able to update invalid user', async () => {
    await expect(
      updateUser.execute({
        user_id: 1,
        name: 'John Doe 2',
        email: 'test2@email.com',
        old_password: 'password',
        password: 'new-password',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should not be able to update email to email already in use by other user', async () => {
    const user = await createUser.execute({
      name: 'John Doe',
      email: 'test@email.com',
      password: 'password',
    });

    await createUser.execute({
      name: 'John Doe',
      email: 'test2@email.com',
      password: 'password',
    });

    await expect(
      updateUser.execute({
        user_id: user.id,
        name: 'John Doe 2',
        email: 'test2@email.com',
        old_password: 'password',
        password: 'new-password',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should not be able to update password if the old password is not informed', async () => {
    const user = await createUser.execute({
      name: 'John Doe',
      email: 'test@email.com',
      password: 'password',
    });

    await expect(
      updateUser.execute({
        user_id: user.id,
        name: 'John Doe 2',
        email: 'test2@email.com',
        password: 'new-password',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should not be able to update password if the old password does not match', async () => {
    const user = await createUser.execute({
      name: 'John Doe',
      email: 'test@email.com',
      password: 'password',
    });

    await expect(
      updateUser.execute({
        user_id: user.id,
        name: 'John Doe 2',
        email: 'test2@email.com',
        old_password: 'wrong-old-password',
        password: 'new-password',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });
});
