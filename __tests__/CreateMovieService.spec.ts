import AppError from '../src/errors/AppError';
import FakeMoviesRepository from '../src/repositories/fakes/FakeMoviesRepository';
import CreateMovieService from '../src/services/CreateMovieService';

let fakeMoviesRepository: FakeMoviesRepository;
let createMovie: CreateMovieService;

describe('CreateMovie', () => {
  beforeEach(() => {
    fakeMoviesRepository = new FakeMoviesRepository();

    createMovie = new CreateMovieService(fakeMoviesRepository);
  });

  it('should be able to create a new movie', async () => {
    const movie = await createMovie.execute({
      title: 'Test movie',
      director: 'John Doe',
      synopsis: 'Some description of the movie',
      cast: JSON.stringify(['Jane Doe', 'John One']),
      genres: JSON.stringify(['Genre 1', 'Genre 2']),
      screenwriters: JSON.stringify(['Jane Three', 'John Four']),
    });

    expect(movie).toHaveProperty('id');
  });

  it('should not be able to create a new movie with the same name of other movie', async () => {
    await createMovie.execute({
      title: 'Test movie',
      director: 'John Doe',
      synopsis: 'Some description of the movie',
      cast: JSON.stringify(['Jane Doe', 'John One']),
      genres: JSON.stringify(['Genre 1', 'Genre 2']),
      screenwriters: JSON.stringify(['Jane Three', 'John Four']),
    });

    await expect(
      createMovie.execute({
        title: 'Test movie',
        director: 'John Doe',
        synopsis: 'Some description of the movie',
        cast: JSON.stringify(['Jane Doe', 'John One']),
        genres: JSON.stringify(['Genre 1', 'Genre 2']),
        screenwriters: JSON.stringify(['Jane Three', 'John Four']),
      }),
    ).rejects.toBeInstanceOf(AppError);
  });
});
