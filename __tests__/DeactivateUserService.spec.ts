import AppError from '../src/errors/AppError';
import FakeUsersRepository from '../src/repositories/fakes/FakeUsersRepository';

import CreateUserService from '../src/services/CreateUserService';
import DeactivateUserService from '../src/services/DeactivateUserService';

let fakeUsersRepository: FakeUsersRepository;
let createUser: CreateUserService;
let deactivateUser: DeactivateUserService;

describe('DeactivateUser', () => {
  beforeEach(() => {
    fakeUsersRepository = new FakeUsersRepository();

    createUser = new CreateUserService(fakeUsersRepository);
    deactivateUser = new DeactivateUserService(fakeUsersRepository);
  });

  it('should be able to deactivate user', async () => {
    const user = await createUser.execute({
      name: 'John Doe',
      email: 'test@email.com',
      password: 'password',
    });

    await deactivateUser.execute(user.id);

    expect(user.deactivated_at).toBeInstanceOf(Date);
  });

  it('should not be able to deactivate an invalid user', async () => {
    await expect(deactivateUser.execute(1)).rejects.toBeInstanceOf(AppError);
  });

  it('should not be able to deactivate an user already deactivated', async () => {
    const user = await createUser.execute({
      name: 'John Doe',
      email: 'test@email.com',
      password: 'password',
    });

    await deactivateUser.execute(user.id);

    await expect(deactivateUser.execute(user.id)).rejects.toBeInstanceOf(
      AppError,
    );
  });
});
