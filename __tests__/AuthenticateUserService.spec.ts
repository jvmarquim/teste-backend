import 'dotenv/config';
import AppError from '../src/errors/AppError';
import FakeUsersRepository from '../src/repositories/fakes/FakeUsersRepository';

import AuthenticateUserService from '../src/services/AuthenticateUserService';
import CreateUserService from '../src/services/CreateUserService';
import DeactivateUserService from '../src/services/DeactivateUserService';

let fakeUsersRepository: FakeUsersRepository;
let authenticateUser: AuthenticateUserService;
let createUser: CreateUserService;
let deactivateUser: DeactivateUserService;

describe('AuthenticateUser', () => {
  beforeEach(() => {
    fakeUsersRepository = new FakeUsersRepository();

    authenticateUser = new AuthenticateUserService(fakeUsersRepository);
    createUser = new CreateUserService(fakeUsersRepository);
    deactivateUser = new DeactivateUserService(fakeUsersRepository);
  });

  it('should be able to login', async () => {
    await createUser.execute({
      name: 'John Doe',
      email: 'test@email.com',
      password: 'password',
    });

    const authenticated = await authenticateUser.execute({
      email: 'test@email.com',
      password: 'password',
    });

    expect(authenticated).toHaveProperty('token');
  });

  it('should not be able to login without valid email', async () => {
    await expect(
      authenticateUser.execute({
        email: 'test@email.com',
        password: 'password',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should not be able to login if the user is deactivate', async () => {
    const user = await createUser.execute({
      name: 'John Doe',
      email: 'test@email.com',
      password: 'password',
    });

    await deactivateUser.execute(user.id);

    await expect(
      authenticateUser.execute({
        email: 'test@email.com',
        password: 'password',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should not be able to login if the password is incorrect', async () => {
    await createUser.execute({
      name: 'John Doe',
      email: 'test@email.com',
      password: 'password',
    });

    await expect(
      authenticateUser.execute({
        email: 'test@email.com',
        password: 'wrong-password',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });
});
