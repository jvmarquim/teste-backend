// import AppError from '../src/errors/AppError';
import FakeMoviesRepository from '../src/repositories/fakes/FakeMoviesRepository';
import FakeVotesRepository from '../src/repositories/fakes/FakeVotesRepository';
import CreateMovieService from '../src/services/CreateMovieService';

import ListMoviesService from '../src/services/ListMoviesService';
import CreateVoteService from '../src/services/CreateVoteService';

let fakeMoviesRepository: FakeMoviesRepository;
let fakeVotesRepository: FakeVotesRepository;
let listMovies: ListMoviesService;
let createMovie: CreateMovieService;
let createVote: CreateVoteService;

describe('ListMovie', () => {
  beforeEach(() => {
    fakeMoviesRepository = new FakeMoviesRepository();
    fakeVotesRepository = new FakeVotesRepository();

    listMovies = new ListMoviesService(
      fakeMoviesRepository,
      fakeVotesRepository,
    );

    createMovie = new CreateMovieService(fakeMoviesRepository);
    createVote = new CreateVoteService(fakeVotesRepository);
  });

  it('should be able to calculate total votes and average grade of movies', async () => {
    const movie = await createMovie.execute({
      title: 'Test movie',
      director: 'John Doe',
      synopsis: 'Some description of the movie',
      cast: JSON.stringify(['Jane Doe', 'John One']),
      genres: JSON.stringify(['Genre 1', 'Genre 2']),
      screenwriters: JSON.stringify(['Jane Three', 'John Four']),
    });

    await createVote.execute({ user_id: 1, movie_id: movie.id, grade: 2 });
    await createVote.execute({ user_id: 2, movie_id: movie.id, grade: 2 });
    await createVote.execute({ user_id: 3, movie_id: movie.id, grade: 3 });

    Object.assign(movie, {
      cast: ['Jane Doe', 'John One'],
      genres: ['Genre 1', 'Genre 2'],
      screenwriters: ['Jane Three', 'John Four'],
    });

    const movies = await listMovies.execute({
      title: undefined,
      director: undefined,
      actor: undefined,
      genre: undefined,
    });

    const testMovie = {
      movie,
      details: {
        total_votes: 3,
        average_grade: Number((7 / 3).toFixed(1)),
      },
    };

    expect(movies).toEqual([testMovie]);
  });

  it('should be able to calculate total votes and average grade of movies without votes', async () => {
    const movie = await createMovie.execute({
      title: 'Test movie',
      director: 'John Doe',
      synopsis: 'Some description of the movie',
      cast: JSON.stringify(['Jane Doe', 'John One']),
      genres: JSON.stringify(['Genre 1', 'Genre 2']),
      screenwriters: JSON.stringify(['Jane Three', 'John Four']),
    });

    Object.assign(movie, {
      cast: ['Jane Doe', 'John One'],
      genres: ['Genre 1', 'Genre 2'],
      screenwriters: ['Jane Three', 'John Four'],
    });

    const movies = await listMovies.execute({
      title: undefined,
      director: undefined,
      actor: undefined,
      genre: undefined,
    });

    const testMovie = {
      movie,
      details: {
        total_votes: 0,
        average_grade: 0,
      },
    };

    expect(movies).toEqual([testMovie]);
  });
});
