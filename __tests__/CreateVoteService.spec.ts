import AppError from '../src/errors/AppError';
import FakeVotesRepository from '../src/repositories/fakes/FakeVotesRepository';

import CreateVoteService from '../src/services/CreateVoteService';

let fakeVotesRepository: FakeVotesRepository;
let createVote: CreateVoteService;

describe('CreateVote', () => {
  beforeEach(() => {
    fakeVotesRepository = new FakeVotesRepository();

    createVote = new CreateVoteService(fakeVotesRepository);
  });

  it('should be able to vote', async () => {
    const vote = await createVote.execute({
      user_id: 1,
      movie_id: 1,
      grade: 2,
    });

    expect(vote).toHaveProperty('id');
  });

  it('should not be allowed for a user to vote two times at the same movie', async () => {
    await createVote.execute({
      user_id: 1,
      movie_id: 1,
      grade: 2,
    });

    await expect(
      createVote.execute({
        user_id: 1,
        movie_id: 1,
        grade: 4,
      }),
    ).rejects.toBeInstanceOf(AppError);
  });
});
