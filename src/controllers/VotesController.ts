import { Request, Response } from 'express';
import { classToClass } from 'class-transformer';

import VotesRepository from '../repositories/typeorm/VotesRepository';
import CreateVoteService from '../services/CreateVoteService';

export default class VotesController {
  public async create(request: Request, response: Response): Promise<Response> {
    const { movie_id } = request.params;
    const { grade } = request.body;
    const { id } = request.user;

    const votesRepository = new VotesRepository();

    const vote = new CreateVoteService(votesRepository);

    const movie = await vote.execute({
      user_id: id,
      movie_id: Number(movie_id),
      grade,
    });

    return response.json(classToClass(movie));
  }
}
