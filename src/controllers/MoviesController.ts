import { Request, Response } from 'express';
import { classToClass } from 'class-transformer';

import MoviesRepository from '../repositories/typeorm/MoviesRepository';
import VotesRepository from '../repositories/typeorm/VotesRepository';

import CreateMovieService from '../services/CreateMovieService';
import ListMoviesService from '../services/ListMoviesService';

export default class MoviesController {
  public async create(request: Request, response: Response): Promise<Response> {
    const {
      title,
      synopsis,
      director,
      screenwriters,
      cast,
      genres,
    } = request.body;

    const moviesRepository = new MoviesRepository();

    const createMovie = new CreateMovieService(moviesRepository);

    const movie = await createMovie.execute({
      title,
      synopsis,
      director,
      screenwriters: JSON.stringify(screenwriters),
      cast: JSON.stringify(cast),
      genres: JSON.stringify(genres),
    });

    return response.json(classToClass(movie));
  }

  public async index(request: Request, response: Response): Promise<Response> {
    const { title, actor, director, genre } = request.query;

    const moviesRepository = new MoviesRepository();
    const votesRepository = new VotesRepository();

    const listMovies = new ListMoviesService(moviesRepository, votesRepository);

    const movies = await listMovies.execute({
      title: title?.toString(),
      actor: actor?.toString(),
      director: director?.toString(),
      genre: genre?.toString(),
    });

    const parsedMovies = movies.map(movie => classToClass(movie));

    return response.json(parsedMovies);
  }
}
