import { Request, Response } from 'express';
import { classToClass } from 'class-transformer';

import UsersRepository from '../repositories/typeorm/UsersRepository';
import CreateUserService from '../services/CreateUserService';
import UpdateUserService from '../services/UpdateUserService';
import DeactivateUserService from '../services/DeactivateUserService';

export default class AdminsController {
  public async create(request: Request, response: Response): Promise<Response> {
    const { name, email, password } = request.body;

    const usersRepository = new UsersRepository();

    const createUser = new CreateUserService(usersRepository);

    const user = await createUser.execute({
      name,
      email,
      password,
      is_admin: true,
    });

    return response.json(classToClass(user));
  }

  public async update(request: Request, response: Response): Promise<Response> {
    const { name, email, old_password, password, is_admin } = request.body;
    const { user_id } = request.params;

    const usersRepository = new UsersRepository();

    const updateUser = new UpdateUserService(usersRepository);

    const user = await updateUser.execute({
      user_id: Number(user_id),
      name,
      email,
      is_admin,
      old_password,
      password,
    });

    return response.json(classToClass(user));
  }

  public async deactivate(
    request: Request,
    response: Response,
  ): Promise<Response> {
    const { user_id } = request.params;

    const usersRepository = new UsersRepository();

    const deactivateUser = new DeactivateUserService(usersRepository);

    await deactivateUser.execute(Number(user_id));

    return response.json({});
  }
}
