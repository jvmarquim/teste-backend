import { Request, Response } from 'express';
import { classToClass } from 'class-transformer';

import CreateUserService from '../services/CreateUserService';
import UpdateUserService from '../services/UpdateUserService';
import DeactivateUserService from '../services/DeactivateUserService';
import UsersRepository from '../repositories/typeorm/UsersRepository';

export default class UsersController {
  public async create(request: Request, response: Response): Promise<Response> {
    const { name, email, password } = request.body;

    const usersRepository = new UsersRepository();

    const createUser = new CreateUserService(usersRepository);

    const user = await createUser.execute({
      name,
      email,
      password,
    });

    return response.json(classToClass(user));
  }

  public async update(request: Request, response: Response): Promise<Response> {
    const { name, email, old_password, password } = request.body;
    const { id } = request.user;

    const usersRepository = new UsersRepository();

    const updateUser = new UpdateUserService(usersRepository);

    const user = await updateUser.execute({
      user_id: id,
      name,
      email,
      old_password,
      password,
    });

    return response.json(classToClass(user));
  }

  public async deactivate(
    request: Request,
    response: Response,
  ): Promise<Response> {
    const { id } = request.user;

    const usersRepository = new UsersRepository();

    const deactivateUser = new DeactivateUserService(usersRepository);

    await deactivateUser.execute(id);

    return response.json({});
  }
}
