import Vote from '../models/Vote';

export interface CreateVote {
  user_id: number;
  movie_id: number;
  grade: number;
}

export interface FindByUserAndMovie {
  user_id: number;
  movie_id: number;
}

export default interface IVotesRepository {
  create(data: CreateVote): Promise<Vote>;
  findAllByMovieId(movie_id: number): Promise<Vote[]>;
  findByUserIdAndMovieId({
    user_id,
    movie_id,
  }: FindByUserAndMovie): Promise<Vote | undefined>;
}
