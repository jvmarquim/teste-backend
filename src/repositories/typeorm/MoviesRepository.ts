import { Repository, getRepository, EntityRepository } from 'typeorm';

import Movie from '../../models/Movie';

import IMoviesRepository, { CreateMovie, Filters } from '../IMoviesRepository';

@EntityRepository()
export default class MoviesRepository implements IMoviesRepository {
  private ormRepository: Repository<Movie>;

  constructor() {
    this.ormRepository = getRepository(Movie);
  }

  public async create({
    title,
    synopsis,
    director,
    screenwriters,
    cast,
    genres,
  }: CreateMovie): Promise<Movie> {
    const movie = this.ormRepository.create({
      title,
      synopsis,
      director,
      screenwriters,
      cast,
      genres,
    });

    await this.ormRepository.save(movie);

    return movie;
  }

  public async save(movie: Movie): Promise<Movie> {
    return this.ormRepository.save(movie);
  }

  public async findByTitle(title: string): Promise<Movie | undefined> {
    const movie = await this.ormRepository.findOne({
      where: { title },
    });

    return movie;
  }

  public async findById(id: number): Promise<Movie | undefined> {
    const movie = await this.ormRepository.findOne(id);

    return movie;
  }

  public async findAll({
    director,
    title,
    genre,
    actor,
  }: Filters): Promise<Movie[]> {
    let queryBuilder = this.ormRepository.createQueryBuilder('movie');

    if (director) {
      queryBuilder = queryBuilder.andWhere(
        'upper(movie.director) = :director',
        {
          director,
        },
      );
    }

    if (title) {
      queryBuilder = queryBuilder.andWhere('upper(movie.title) = :title', {
        title,
      });
    }

    if (genre) {
      queryBuilder = queryBuilder.andWhere('upper(movie.genres) like :genre', {
        genre: `%${genre}%`,
      });
    }

    if (actor) {
      queryBuilder = queryBuilder.andWhere('upper(movie.cast) like :actor', {
        actor: `%${actor}%`,
      });
    }

    const movies = await queryBuilder.orderBy('movie.title', 'ASC').getMany();

    return movies;
  }
}
