import { Repository, getRepository, EntityRepository } from 'typeorm';

import IVotesRepository, {
  CreateVote,
  FindByUserAndMovie,
} from '../IVotesRepository';
import Vote from '../../models/Vote';

@EntityRepository()
export default class VotesRepository implements IVotesRepository {
  private ormRepository: Repository<Vote>;

  constructor() {
    this.ormRepository = getRepository(Vote);
  }

  public async create({ user_id, movie_id, grade }: CreateVote): Promise<Vote> {
    const vote = this.ormRepository.create({
      user_id,
      movie_id,
      grade,
    });

    await this.ormRepository.save(vote);

    return vote;
  }

  public async findAllByMovieId(movie_id: number): Promise<Vote[]> {
    const votes = await this.ormRepository.find({ where: { movie_id } });

    return votes;
  }

  public async findByUserIdAndMovieId({
    user_id,
    movie_id,
  }: FindByUserAndMovie): Promise<Vote | undefined> {
    const vote = await this.ormRepository.findOne({
      where: { user_id, movie_id },
    });

    return vote;
  }
}
