import { Repository, getRepository, EntityRepository } from 'typeorm';

import User from '../../models/User';
import IUsersRepository, { CreateUser } from '../IUsersRepository';

@EntityRepository()
export default class UsersRepository implements IUsersRepository {
  private ormRepository: Repository<User>;

  constructor() {
    this.ormRepository = getRepository(User);
  }

  public async create({
    name,
    email,
    password,
    is_admin,
  }: CreateUser): Promise<User> {
    const user = this.ormRepository.create({
      name,
      email,
      password,
      is_admin,
    });

    await this.ormRepository.save(user);

    return user;
  }

  public async save(user: User): Promise<User> {
    return this.ormRepository.save(user);
  }

  public async findById(id: number): Promise<User | undefined> {
    const user = await this.ormRepository.findOne(id);

    return user;
  }

  public async findByEmail(email: string): Promise<User | undefined> {
    const user = await this.ormRepository.findOne({ where: { email } });

    return user;
  }
}
