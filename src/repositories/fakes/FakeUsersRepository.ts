import User from '../../models/User';
import IUsersRepository, { CreateUser } from '../IUsersRepository';

export default class FakeUsersRepository implements IUsersRepository {
  private users: User[] = [];

  public async create({
    email,
    name,
    password,
    is_admin,
  }: CreateUser): Promise<User> {
    const user = new User();

    Object.assign(user, {
      id: Math.floor(Math.random() * 100) + 1,
      email,
      name,
      password,
      is_admin,
    });

    this.users.push(user);

    return user;
  }

  public async save(user: User): Promise<User> {
    const existingUserIndex = this.users.findIndex(
      existingUser => existingUser.id === user.id,
    );

    this.users.splice(existingUserIndex, 1, user);

    return user;
  }

  public async findById(id: number): Promise<User | undefined> {
    const findUser = this.users.find(user => user.id === id);

    return findUser;
  }

  public async findByEmail(email: string): Promise<User | undefined> {
    const findUser = this.users.find(user => user.email === email);

    return findUser;
  }
}
