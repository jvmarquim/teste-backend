import Movie from '../../models/Movie';
import IMoviesRepository, { CreateMovie, Filters } from '../IMoviesRepository';

export default class FakeMoviesRepository implements IMoviesRepository {
  private movies: Movie[] = [];

  public async create({
    title,
    synopsis,
    director,
    screenwriters,
    cast,
    genres,
  }: CreateMovie): Promise<Movie> {
    const movie = new Movie();

    Object.assign(movie, {
      id: Math.floor(Math.random() * 100) + 1,
      title,
      synopsis,
      director,
      screenwriters: JSON.stringify(screenwriters),
      cast: JSON.stringify(cast),
      genres: JSON.stringify(genres),
    });

    this.movies.push(movie);

    return movie;
  }

  public async save(movie: Movie): Promise<Movie> {
    const existingMovieIndex = this.movies.findIndex(
      existingMovie => existingMovie.id === movie.id,
    );

    this.movies.splice(existingMovieIndex, 1, movie);

    return movie;
  }

  public async findById(id: number): Promise<Movie | undefined> {
    const findMovie = this.movies.find(movie => movie.id === id);

    return findMovie;
  }

  public async findByTitle(title: string): Promise<Movie | undefined> {
    const findMovie = this.movies.find(movie => movie.title === title);

    return findMovie;
  }

  public async findAll({
    title,
    actor,
    director,
    genre,
  }: Filters): Promise<Movie[]> {
    let filteredMovies = this.movies;

    if (title) {
      filteredMovies = filteredMovies.filter(
        movie => movie.title.toUpperCase() === title.toUpperCase(),
      );
    }

    if (director) {
      filteredMovies = filteredMovies.filter(
        movie => movie.director.toUpperCase() === director.toUpperCase(),
      );
    }

    if (actor) {
      filteredMovies = filteredMovies.filter(movie => {
        const parsedCast: string[] = JSON.parse(movie.cast);

        return parsedCast.find(
          member => member.toUpperCase() === actor.toUpperCase(),
        );
      });
    }

    if (genre) {
      filteredMovies = filteredMovies.filter(movie => {
        const parsedGenres: string[] = JSON.parse(movie.genres);

        return parsedGenres.find(
          genreType => genreType.toUpperCase() === genre.toUpperCase(),
        );
      });
    }

    return filteredMovies;
  }
}
