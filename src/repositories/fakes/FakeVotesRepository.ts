import Vote from '../../models/Vote';
import IVotesRepository, {
  CreateVote,
  FindByUserAndMovie,
} from '../IVotesRepository';

export default class FakeVotesRepository implements IVotesRepository {
  private votes: Vote[] = [];

  public async create({ user_id, movie_id, grade }: CreateVote): Promise<Vote> {
    const vote = new Vote();

    Object.assign(vote, {
      id: Math.floor(Math.random() * 100) + 1,
      movie_id,
      user_id,
      grade,
    });

    this.votes.push(vote);

    return vote;
  }

  public async findAllByMovieId(movie_id: number): Promise<Vote[]> {
    const findVotes = this.votes.filter(vote => vote.movie_id === movie_id);

    return findVotes;
  }

  public async findByUserIdAndMovieId({
    movie_id,
    user_id,
  }: FindByUserAndMovie): Promise<Vote | undefined> {
    const findVote = this.votes.find(
      vote => vote.movie_id === movie_id && vote.user_id === user_id,
    );

    return findVote;
  }
}
