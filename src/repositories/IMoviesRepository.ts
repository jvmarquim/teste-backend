import Movie from '../models/Movie';

export interface CreateMovie {
  title: string;
  synopsis: string;
  director: string;
  screenwriters: string;
  cast: string;
  genres: string;
}

export interface Filters {
  director: string | undefined;
  title: string | undefined;
  genre: string | undefined;
  actor: string | undefined;
}

export default interface IMoviesRepository {
  create(data: CreateMovie): Promise<Movie>;
  save(movie: Movie): Promise<Movie>;
  findAll(data: Filters): Promise<Movie[]>;
  findByTitle(title: string): Promise<Movie | undefined>;
  findById(id: number): Promise<Movie | undefined>;
}
