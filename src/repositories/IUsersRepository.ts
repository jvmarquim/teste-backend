import User from '../models/User';

export interface CreateUser {
  name: string;
  email: string;
  password: string;
  is_admin?: boolean;
}

export default interface IUsersRepository {
  create(data: CreateUser): Promise<User>;
  save(user: User): Promise<User>;
  findById(id: number): Promise<User | undefined>;
  findByEmail(email: string): Promise<User | undefined>;
}
