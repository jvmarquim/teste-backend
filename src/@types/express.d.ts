declare namespace Express {
  export interface Request {
    user: {
      id: number;
      is_admin: boolean;
    };
  }
}
