import { hash, compare } from 'bcryptjs';

import AppError from '../errors/AppError';

import User from '../models/User';
import IUsersRepository from '../repositories/IUsersRepository';

interface Request {
  user_id: number;
  name: string;
  email: string;
  is_admin?: boolean;
  old_password?: string;
  password?: string;
}

export default class UpdateUserService {
  constructor(private usersRepository: IUsersRepository) {}

  public async execute({
    user_id,
    name,
    email,
    is_admin,
    old_password,
    password,
  }: Request): Promise<User> {
    const user = await this.usersRepository.findById(user_id);

    if (!user) {
      throw new AppError('User not found.');
    }

    const existingUserWithRequestedEmail = await this.usersRepository.findByEmail(
      email,
    );

    if (
      existingUserWithRequestedEmail &&
      existingUserWithRequestedEmail.id !== user_id
    ) {
      throw new AppError('Email address already used.', 409);
    }

    user.name = name || user.name;
    user.email = email || user.email;

    if (password && !old_password) {
      throw new AppError(
        'You need to inform the old password to set a new password.',
      );
    }

    if (password && old_password) {
      const checkOldPassword = await compare(old_password, user.password);

      if (!checkOldPassword) {
        throw new AppError('Old password does not match.');
      }

      user.password = await hash(password, 8);
    }

    await this.usersRepository.save(user);

    return user;
  }
}
