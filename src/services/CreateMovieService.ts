import AppError from '../errors/AppError';

import Movie from '../models/Movie';
import IMoviesRepository from '../repositories/IMoviesRepository';

interface Request {
  title: string;
  synopsis: string;
  director: string;
  screenwriters: string;
  cast: string;
  genres: string;
}

export default class CreateMovieService {
  constructor(private moviesRepository: IMoviesRepository) {}

  public async execute({
    title,
    synopsis,
    director,
    screenwriters,
    cast,
    genres,
  }: Request): Promise<Movie> {
    const existingMovieTitle = await this.moviesRepository.findByTitle(title);

    if (existingMovieTitle) {
      throw new AppError('A movie with this title already exists.', 409);
    }

    const movie = await this.moviesRepository.create({
      title,
      synopsis,
      director,
      screenwriters,
      cast,
      genres,
    });

    return movie;
  }
}
