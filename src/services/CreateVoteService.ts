import AppError from '../errors/AppError';

import Vote from '../models/Vote';
import IVotesRepository from '../repositories/IVotesRepository';

interface Request {
  user_id: number;
  movie_id: number;
  grade: 0 | 1 | 2 | 3 | 4;
}

export default class VoteService {
  constructor(private votesRepository: IVotesRepository) {}

  public async execute({ user_id, movie_id, grade }: Request): Promise<Vote> {
    const userAlreadyVoted = await this.votesRepository.findByUserIdAndMovieId({
      user_id,
      movie_id,
    });

    if (userAlreadyVoted) {
      throw new AppError("You can't vote two times on the same movie.");
    }

    const vote = await this.votesRepository.create({
      user_id,
      movie_id,
      grade,
    });

    return vote;
  }
}
