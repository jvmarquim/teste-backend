import Movie from '../models/Movie';
import IMoviesRepository from '../repositories/IMoviesRepository';
import IVotesRepository from '../repositories/IVotesRepository';

interface Request {
  title: string | undefined;
  director: string | undefined;
  actor: string | undefined;
  genre: string | undefined;
}

interface Response {
  movie: Movie;
  details: {
    total_votes: number;
    average_grade: number;
  };
}

export default class ListMoviesService {
  constructor(
    private moviesRepository: IMoviesRepository,
    private votesRepository: IVotesRepository,
  ) {}

  public async execute({
    title,
    director,
    actor,
    genre,
  }: Request): Promise<Response[]> {
    const movies = await this.moviesRepository.findAll({
      title: title?.toString().toUpperCase(),
      actor: actor?.toString().toUpperCase(),
      director: director?.toString().toUpperCase(),
      genre: genre?.toString().toUpperCase(),
    });

    const moviesWithDetails = [];

    for (const movie of movies) {
      const votes = await this.votesRepository.findAllByMovieId(movie.id);

      if (!votes.length) {
        moviesWithDetails.push({
          movie,
          details: {
            average_grade: 0,
            total_votes: 0,
          },
        });
      } else {
        const total_votes = votes.length;
        const addedGrades = votes.reduce(
          (accumulator, vote) => accumulator + vote.grade,
          0,
        );
        const average_grade = addedGrades / total_votes;

        moviesWithDetails.push({
          movie,
          details: {
            average_grade: Number(average_grade.toFixed(1)),
            total_votes,
          },
        });
      }
    }

    return moviesWithDetails;
  }
}
