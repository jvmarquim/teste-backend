import { hash } from 'bcryptjs';

import AppError from '../errors/AppError';

import User from '../models/User';
import IUsersRepository from '../repositories/IUsersRepository';

interface Request {
  name: string;
  email: string;
  password: string;
  is_admin?: boolean;
}

export default class CreateUserService {
  constructor(private usersRepository: IUsersRepository) {}

  public async execute({
    name,
    email,
    password,
    is_admin,
  }: Request): Promise<User> {
    const existingUserWithRequestedEmail = await this.usersRepository.findByEmail(
      email,
    );

    if (existingUserWithRequestedEmail) {
      throw new AppError('Email address already used.', 409);
    }

    const hashedPassword = await hash(password, 8);

    const user = await this.usersRepository.create({
      name,
      email,
      password: hashedPassword,
      is_admin,
    });

    return user;
  }
}
