import AppError from '../errors/AppError';
import IUsersRepository from '../repositories/IUsersRepository';

export default class DeactivateUserService {
  constructor(private usersRepository: IUsersRepository) {}

  public async execute(id: number): Promise<void> {
    const user = await this.usersRepository.findById(Number(id));

    if (!user) {
      throw new AppError('User not found.');
    }

    if (user.deactivated_at) {
      throw new AppError('User not found.');
    }

    user.deactivated_at = new Date(Date.now());

    this.usersRepository.save(user);
  }
}
