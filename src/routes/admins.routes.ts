import { Router } from 'express';
import { celebrate, Segments, Joi } from 'celebrate';

import AdminsController from '../controllers/AdminsController';

import ensureUserAuthenticated from '../middlewares/ensureUserAuthenticated';
import onlyAdminsAllowed from '../middlewares/onlyAdminsAllowed';

const adminsRouter = Router();
const adminsController = new AdminsController();

adminsRouter.post(
  '/',
  celebrate({
    [Segments.BODY]: {
      name: Joi.string().required(),
      email: Joi.string().required().email(),
      password: Joi.string().required(),
      password_confirmation: Joi.string().required().valid(Joi.ref('password')),
    },
  }),
  adminsController.create,
);

adminsRouter.put(
  '/:user_id',
  ensureUserAuthenticated,
  onlyAdminsAllowed,
  celebrate({
    [Segments.BODY]: {
      name: Joi.string().required(),
      email: Joi.string().required().email(),
      old_password: Joi.string(),
      password: Joi.string(),
      is_admin: Joi.boolean(),
      password_confirmation: Joi.string().when('password', {
        is: Joi.exist(),
        then: Joi.required().valid(Joi.ref('password')),
      }),
    },
  }),
  adminsController.update,
);

adminsRouter.patch(
  '/deactivate/:user_id',
  ensureUserAuthenticated,
  onlyAdminsAllowed,
  adminsController.deactivate,
);

export default adminsRouter;
