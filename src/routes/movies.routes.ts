import { Router } from 'express';
import { celebrate, Segments, Joi } from 'celebrate';

import MoviesController from '../controllers/MoviesController';
import VotesController from '../controllers/VotesController';

import ensureUserAuthenticated from '../middlewares/ensureUserAuthenticated';
import onlyRegularUsersAllowed from '../middlewares/onlyRegularUsersAllowed';
import onlyAdminsAllowed from '../middlewares/onlyAdminsAllowed';

const moviesRouter = Router();
const moviesController = new MoviesController();
const votesController = new VotesController();

moviesRouter.use(ensureUserAuthenticated);

moviesRouter.get(
  '/',
  celebrate({
    [Segments.QUERY]: {
      title: Joi.string(),
      actor: Joi.string(),
      director: Joi.string(),
      genre: Joi.string(),
    },
  }),
  moviesController.index,
);

moviesRouter.post(
  '/',
  onlyAdminsAllowed,
  celebrate({
    [Segments.BODY]: {
      title: Joi.string().required(),
      synopsis: Joi.string().required(),
      director: Joi.string().required(),
      screenwriters: Joi.array().items(Joi.string()).required(),
      cast: Joi.array().items(Joi.string()).required(),
      genres: Joi.array().items(Joi.string()).required(),
    },
  }),
  moviesController.create,
);

moviesRouter.post(
  '/vote/:movie_id',
  onlyRegularUsersAllowed,
  celebrate({
    [Segments.PARAMS]: {
      movie_id: Joi.number().integer().required(),
    },
    [Segments.BODY]: {
      grade: Joi.number().integer().min(0).max(4),
    },
  }),
  votesController.create,
);

export default moviesRouter;
