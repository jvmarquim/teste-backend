import { Request, Response, NextFunction } from 'express';

import AppError from '../errors/AppError';

export default async function onlyRegularUsersAllowed(
  request: Request,
  response: Response,
  next: NextFunction,
): Promise<void> {
  const { user } = request;

  if (user.is_admin) {
    throw new AppError('You are not allowed to vote.', 403);
  }

  return next();
}
