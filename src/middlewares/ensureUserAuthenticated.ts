import { Request, Response, NextFunction } from 'express';
import { getCustomRepository } from 'typeorm';
import { verify } from 'jsonwebtoken';
import authConfig from '../config/auth';

import AppError from '../errors/AppError';

import UsersRepository from '../repositories/typeorm/UsersRepository';

interface TokenPayload {
  iat: number;
  exp: number;
  sub: string;
}

export default async function ensureUserAuthenticated(
  request: Request,
  response: Response,
  next: NextFunction,
): Promise<void> {
  const authHeader = request.headers.authorization;

  if (!authHeader) {
    throw new AppError('Missing JWT token.', 401);
  }

  const [, token] = authHeader.split(' ');

  const { secret } = authConfig.jwt;

  try {
    const decoded = verify(token, secret);

    const { sub } = decoded as TokenPayload;

    const userId = sub;

    const usersRepository = getCustomRepository(UsersRepository);

    const user = await usersRepository.findById(Number(userId));

    if (!user) {
      throw new AppError('User not found.', 401);
    }

    if (user.deactivated_at) {
      throw new AppError('User not found.', 401);
    }

    request.user = {
      id: user.id,
      is_admin: user.is_admin,
    };

    return next();
  } catch (err) {
    console.log(err);
    throw new AppError('Invalid JWT token.', 401);
  }
}
