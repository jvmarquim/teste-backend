import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export default class AddDeactivatedAtFieldToUsers1602379395249
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'users',
      new TableColumn({
        name: 'deactivated_at',
        type: 'timestamp',
        isNullable: true,
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('users', 'deactivated_at');
  }
}
