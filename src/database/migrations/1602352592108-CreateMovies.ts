import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export default class CreateMovies1602352592108 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'movies',
        columns: [
          {
            name: 'id',
            type: 'serial',
            isPrimary: true,
            isUnique: true,
            generationStrategy: 'increment',
          },
          {
            name: 'title',
            type: 'varchar',
            isUnique: true,
          },
          {
            name: 'synopsis',
            type: 'varchar',
          },
          {
            name: 'director',
            type: 'varchar',
          },
          {
            name: 'screenwriters',
            type: 'varchar',
          },
          {
            name: 'cast',
            type: 'varchar',
          },
          {
            name: 'genres',
            type: 'varchar',
          },
          {
            name: 'total_votes',
            type: 'int',
            default: 0,
          },
          {
            name: 'average_grade',
            type: 'decimal',
            precision: 10,
            scale: 1,
            default: 0,
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'now()',
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'now()',
          },
        ],
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('movies');
  }
}
