import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export default class DropTotalVotesAndAverageGradeFromMovies1602428976590
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('movies', 'average_grade');
    await queryRunner.dropColumn('movies', 'total_votes');
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'movies',
      new TableColumn({
        name: 'total_votes',
        type: 'int',
        default: 0,
      }),
    );

    await queryRunner.addColumn(
      'movies',
      new TableColumn({
        name: 'average_grade',
        type: 'decimal',
        precision: 10,
        scale: 1,
        default: 0,
      }),
    );
  }
}
