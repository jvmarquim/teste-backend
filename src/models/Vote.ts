import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';

import { Exclude } from 'class-transformer';

import User from './User';
import Movie from './Movie';

@Entity('votes')
class Vote {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  user_id: number;

  @ManyToOne(() => User, (user: User) => user.votes)
  @JoinColumn({ name: 'user_id' })
  user: User;

  @Column()
  movie_id: number;

  @ManyToOne(() => Movie, (movie: Movie) => movie.votes)
  @JoinColumn({ name: 'movie_id' })
  movie: Movie;

  @Column()
  grade: number;

  @CreateDateColumn()
  @Exclude()
  created_at: Date;

  @UpdateDateColumn()
  @Exclude()
  updated_at: Date;
}

export default Vote;
