import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';

import { Exclude, Expose } from 'class-transformer';
import Vote from './Vote';

@Entity('movies')
class Movie {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  title: string;

  @Column()
  synopsis: string;

  @Column()
  director: string;

  @Column()
  @Exclude()
  screenwriters: string;

  @Expose({ name: 'screenwriters' })
  parseScreenwriters(): string[] {
    return JSON.parse(this.screenwriters);
  }

  @Column()
  @Exclude()
  cast: string;

  @Expose({ name: 'cast' })
  parseCast(): string[] {
    return JSON.parse(this.cast);
  }

  @Column()
  @Exclude()
  genres: string;

  @Expose({ name: 'genres' })
  parseGenres(): string[] {
    return JSON.parse(this.genres);
  }

  @OneToMany(() => Vote, (vote: Vote) => vote.movie)
  votes: Vote[];

  @CreateDateColumn()
  @Exclude()
  created_at: Date;

  @UpdateDateColumn()
  @Exclude()
  updated_at: Date;
}

export default Movie;
