# Sobre

Estes documento README tem como objetivo fornecer as informações necessárias para executar o projeto.

# Sobre o projeto

- Esta é uma API  que o site [IMDb](https://www.imdb.com/) irá consultar para exibir seu conteúdo. A API foi construída utilizando NodeJS, especificamente o framework ExpressJS, com Typescript. O banco de dados utilizado é o PostgreSQL e a comunicação com a base de dados é feita por meio do TypeORM. Para realizar os testes unitários foi utilizado o framework JEST. A validação das rotas é feita utilizando o Celebrate. O projeto contém um arquivo chamado `ioasysTest.postman_collection.json`, que é uma coleção do Postman e pode ser usada para testar as rotas do sistema.

# Requisitos para executar a API

- Ter o NodeJS instalado na sua máquina
- Ter o PostgreSQL instalado na sua máquina ou rodando em um container Docker

# Executando a API

- Clone ou faça o download do projeto na sua máquina
- Execute o PostgreSQL na porta 5432 e crie um banco de dados chamado ioasys_test
- Crie um arquivo `.env` na pasta raiz do projeto e copie o conteúdo contido em `.env.example`. Para criar o secret da aplicação pode ser usado o site [md5.org](https://www.md5online.org/). No arquivo `.env` também podem ser alteradas as configurações de conexão com o banco de dados caso você deseje.
- No diretório do projeto execute o comando `yarn` para instalar as dependências
- Com o banco de dados rodando digite o comando `yarn typeorm migration:run` para executar as migrations e criar as tabelas do banco.
- Finalmente use o comando `yarn start` para executar a aplicação. O servidor irá rodar na porta `3333`.

# Executando os testes

- Para executar os testes unitários basta digitar o comando `yarn test`.

